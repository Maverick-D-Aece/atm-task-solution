package com.progressoft.induction.atm.implementations;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.progressoft.induction.atm.ATM;
import com.progressoft.induction.atm.Banknote;
import com.progressoft.induction.atm.exceptions.InsufficientFundsException;
import com.progressoft.induction.atm.exceptions.NotEnoughMoneyInATMException;

/**
 * A class to implement the ATM interface and initialize the banknotes in an ATM
 * 
 * @author Maverick D. Aece
 */
public class ATMImpl implements ATM {

	private final static BigDecimal FIFTY = new BigDecimal(50);
	private final static BigDecimal TWENTY = new BigDecimal(20);
	private final static BigDecimal TEN = new BigDecimal(10);
	private final static BigDecimal FIVE = new BigDecimal(5);

	private List<Banknote> notesInATM;
	private BigDecimal money;
	private BankingSystemImpl bankingSystem;

	public ATMImpl(List<Banknote> notesInATM) {
		this.notesInATM = notesInATM;
		this.money = calculateTotal();
		this.bankingSystem = new BankingSystemImpl();
	}

	private BigDecimal calculateTotal() {
		if (notesInATM.isEmpty())
			return new BigDecimal(0);		
		return notesInATM.stream().map(Banknote::getValue).reduce(BigDecimal::add).orElse(BigDecimal.ZERO);
	}

	@Override
	public List<Banknote> withdraw(String accountNumber, BigDecimal amount)
			throws NotEnoughMoneyInATMException, InsufficientFundsException {
		List<Banknote> withdrawNotes = new ArrayList<Banknote>();
		if (bankingSystem.getAccountBalance(accountNumber).compareTo(amount) < 0)
			throw new InsufficientFundsException(accountNumber);		
		if (money.compareTo(amount) < 0)
			throw new NotEnoughMoneyInATMException();		
		bankingSystem.debitAccount(accountNumber, amount);
		while (amount.compareTo(FIFTY) >= 0 && notesInATM.contains(Banknote.FIFTY_JOD)) {
			withdrawNotes.add(Banknote.FIFTY_JOD);
			amount = amount.subtract(FIFTY);
			notesInATM.remove(Banknote.FIFTY_JOD);
		}
		while (amount.compareTo(TWENTY) >= 0 && notesInATM.contains(Banknote.TWENTY_JOD)) {
			withdrawNotes.add(Banknote.TWENTY_JOD);
			amount = amount.subtract(TWENTY);
			notesInATM.remove(Banknote.TWENTY_JOD);
		}
		while (amount.compareTo(TEN) >= 0 && notesInATM.contains(Banknote.TEN_JOD)) {
			withdrawNotes.add(Banknote.TEN_JOD);
			amount = amount.subtract(TEN);
			notesInATM.remove(Banknote.TEN_JOD);
		}
		while (amount.compareTo(FIVE) >= 0 && notesInATM.contains(Banknote.FIVE_JOD)) {
			withdrawNotes.add(Banknote.FIVE_JOD);
			amount = amount.subtract(FIVE);
			notesInATM.remove(Banknote.FIVE_JOD);
		}
		money = calculateTotal();
		return withdrawNotes;
	}

	public List<Banknote> getNotesInATM() {
		return notesInATM;
	}

	public void setNotesInATM(List<Banknote> notesInATM) {
		this.notesInATM = notesInATM;
	}

	public BigDecimal getMoney() {
		return money;
	}

	public void setMoney(BigDecimal money) {
		this.money = money;
	}
}
