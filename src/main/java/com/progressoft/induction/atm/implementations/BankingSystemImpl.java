package com.progressoft.induction.atm.implementations;

import java.math.BigDecimal;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.progressoft.induction.atm.BankingSystem;
import com.progressoft.induction.atm.exceptions.AccountNotFoundException;

/**
 * A class to implement the BankingSystem interface and initialize the accounts in a 
 * Banking System.
 * 
 * @author Maverick D. Aece
 */
public class BankingSystemImpl implements BankingSystem {

	private final static BigDecimal INIT_BALANCE = new BigDecimal(1000);
	private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	private Map<String, BigDecimal> accounts = new TreeMap<String, BigDecimal>();
	
	public BankingSystemImpl() {
		addAccount("123456789");
		addAccount("111111111");
		addAccount("222222222");
		addAccount("333333333");
		addAccount("444444444");
	}

	public boolean addAccount(String accountNumber) {
		return addAccount(accountNumber, INIT_BALANCE);
	}

	public boolean addAccount(String accountNumber, BigDecimal amount) {
		if (!accounts.containsKey(accountNumber)) {
			accounts.put(accountNumber, amount);
			return true;
		}
		return false;
	}

	@Override
	public BigDecimal getAccountBalance(String accountNumber) throws AccountNotFoundException {
		if (accounts.containsKey(accountNumber)) {
			return accounts.get(accountNumber);
		}
		throw new AccountNotFoundException(accountNumber);
	}

	@Override
	public void debitAccount(String accountNumber, BigDecimal amount) throws AccountNotFoundException {
		if (accounts.containsKey(accountNumber)) {
			BigDecimal oldAmount = accounts.get(accountNumber);
			BigDecimal newAmount = oldAmount.subtract(amount);
			accounts.put(accountNumber, newAmount);
			LOGGER.log(Level.FINE,
					amount + " debited from Account (" + accountNumber + 
					")\nCurrent balance: " + newAmount);
		} else {
			throw new AccountNotFoundException(accountNumber);
		}

	}

}
