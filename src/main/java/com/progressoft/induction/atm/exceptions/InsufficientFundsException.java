package com.progressoft.induction.atm.exceptions;

import java.util.logging.Level;
import java.util.logging.Logger;

public class InsufficientFundsException extends RuntimeException {

	private final static Logger LOGGER = Logger.getLogger("InsufficientFundsException");

	public InsufficientFundsException(String accountNumber) {
		LOGGER.log(Level.WARNING, "Account (" + accountNumber + 
				") does not have enough funds.");
	}
}
