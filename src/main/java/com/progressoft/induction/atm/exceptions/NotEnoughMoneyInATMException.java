package com.progressoft.induction.atm.exceptions;

import java.util.logging.Level;
import java.util.logging.Logger;

public class NotEnoughMoneyInATMException extends RuntimeException {

	private final static Logger LOGGER = Logger.getLogger("NotEnoughMoneyInATMException");

	public NotEnoughMoneyInATMException() {
		LOGGER.log(Level.WARNING, "There isn't enough money in the ATM.");
	}
}
