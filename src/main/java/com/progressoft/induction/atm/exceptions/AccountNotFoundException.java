package com.progressoft.induction.atm.exceptions;

import java.util.logging.Level;
import java.util.logging.Logger;

public class AccountNotFoundException extends RuntimeException {
	
	private final static Logger LOGGER = Logger.getLogger("AccountNotFoundException");
	
	public AccountNotFoundException(String accountNumber) {
		LOGGER.log(Level.WARNING, "Account (" + accountNumber + 
				") does not exist.");
	}
}
